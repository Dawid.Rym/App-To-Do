function addNewTask(title){
    let taskLi = document.createElement('li');
    taskLi.classList.add('single-task');
    taskLi.innerHTML = TaskHTMLprepair(title);
    
    //Events checked and remove 
    let checkedTaskBtn = taskLi.querySelector('.checked-complete-btn')
    let deleteTaskBtn = taskLi.querySelector('.delete-task-btn')
   


    checkedTaskBtn.addEventListener('click', ()=>{
            let backgroundCheckedBtn = taskLi.querySelector('.background-change');

        checkedTaskBtn.classList.toggle('btn-success')
        if(checkedTaskBtn.classList.contains('btn-success')){
            backgroundCheckedBtn.style.backgroundColor = "#6cce5c";
            backgroundCheckedBtn.style.border = "none";
            backgroundCheckedBtn.style.color = "#fff"
        }else{
            backgroundCheckedBtn.style.backgroundColor = "#fff";
            backgroundCheckedBtn.style.border = "none";
            backgroundCheckedBtn.style.color = "#111"
        }
    } )
     
    //DELETING TASK

    
   
   $(deleteTaskBtn).click(function(){
    let el = deleteTaskBtn.closest('li');
    deleteTaskBtn.closest('ul').removeChild(el)
   })
        
    
    


    //add task to DOM (ul)
    taskContainer.appendChild(taskLi);
    
};
function TaskHTMLprepair(title){
    return ` <div class="input-group">
    <span class="input-group-btn">
    <button class="btn btn-default checked-complete-btn">
    <i class="fas fa-check"></i>
    </button>
    </span>
    <input type="text" class="form-control background-change" placeholder="Task name..." value="${title}">
    <span class="input-group-btn">
    <button class="btn btn-danger delete-task-btn" id="removeTask"><i class="fas fa-times"></i></button>
    </span>
    </div>`
};

//Add new task events
function taskEvents(){
    
    btnTaskForm.addEventListener('submit', (e)=>{
        e.preventDefault();
        if(taskValue.value == ""){
            true; 
            emptyTask.innerHTML= "Your task can't be empty"
        }else{
            addNewTask(taskValue.value) 
            emptyTask.innerHTML= ""
        }
        btnTaskForm.reset(); // clear input after submit
        
    });
};
